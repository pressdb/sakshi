# Import Packages
# Base Packages
import json

## Installed Packages
from envparse import env

# Neo4j Credentials
uri = env.str('GRAPHENEDB_BOLT_URL')
user = env.str('GRAPHENEDB_BOLT_USER')
pwd = env.str('GRAPHENEDB_BOLT_PASSWORD')
path = env.str('NEO4J_CRED_PATH')

config = {"NEO4J_URL": uri,"NEO4J_USR": user,"NEO4J_PWD": pwd}
with open(path, 'w') as f:
    json.dump(config, f)