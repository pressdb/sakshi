# Import Packages
## Standard Packages
import json
from datetime import datetime

## Installed Packages
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from pymongo import MongoClient
from envparse import env

## Custom Packages
import wikipedia_autocomplete # Installed from `wikipedia_autocomplete` repo
import Keyword_Graph as kg

# Dash
external_stylesheets = [
    'https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css'
]
external_scripts = [
     "https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.15.0/lodash.min.js",
     "https://unpkg.com/semantic-ui-react/dist/umd/semantic-ui-react.min.js",
     "https://unpkg.com/axios/dist/axios.min.js",
     "https://cdn.neo4jlabs.com/neovis.js/v1.2.1/neovis.js"
]
app = dash.Dash(__name__, 
                external_scripts=external_scripts,
                external_stylesheets=external_stylesheets, 
                title="Sakshi - PressDB Writing Discovery Engine")
server = app.server

# Record time that user starts survey
landed_at = datetime.utcnow().isoformat()

# Connect to JSON database for storing query metadata
uri = env.str('MONGODB_URI')
client_name = env.str('MONGODB_CLIENT')
client = MongoClient(uri, retryWrites=False)
db = client[client_name]
collection = db['sakshi_queries']

# HTML/CSS
## Note that much of this CSS comes from https://codepen.io/chriddyp/pen/bWLwgP.css
survey_header_font_style = {'textAlign':'center','font-family': "Martel", 
                            'font-size': '24px', 'letter-spacing': '-.1rem'}
survey_question_font_style = {'textAlign':'left','font-family': "Martel", 
                              'font-size': '24px', 'line-height': '0px',
                              'top': '3rem','left': '0', 'letter-spacing': '-.1rem'}
survey_image_style = {'textAlign':'center',"display": "block","margin-left":"auto","margin-right":"auto",'padding-top': 20}
survey_input_style = {'height': '38px', 'padding': '6px 10px',
                      'background-color': '#fff', 'border': '1px solid #D1D1D1',
                      'border-radius': '4px', 'box-shadow': 'none',
                      'box-sizing': 'border-box'}
survey_button_style = {'display': 'inline-block',
  'height': '38px',
  'padding': '0 30px',
  'color': '#555',
  'text-align': 'center',
  'font-weight': '600',
  'line-height': '38px',
  'letter-spacing': '.1rem',
  'text-decoration': 'none',
  'white-space': 'nowrap',
  'background-color': 'transparent',
  'border-radius': '4px',
  'border': '1px solid #bbb',
  'cursor': 'pointer',
  'box-sizing': 'border-box'}
div_style = {
            'font-size': '1em',
            'line-height': '0px',
            'margin': '3',
            'margin-bottom':'3rem',
            'position': 'relative',
            'top': '3rem',
            'left': '0',
            'textAlign': 'left',
            'padding-top': '1rem',
            'padding-left':'5rem'}

app.layout = html.Div([
    html.H1([html.Img(src=app.get_asset_url('PressDB_Logo.png'), style = {'height': '35%', 'width': '35%'})], style = survey_image_style),
    html.H1("A streamlined & fun way to better ideas for writing", style = survey_header_font_style),
    html.H1("Let's get started!", style = survey_header_font_style),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),

  
    html.H1("Oh, hey there! What are you looking for? Can you give us a few keywords? ", style = survey_question_font_style),
    wikipedia_autocomplete.WikipediaAutocomplete(id = 'keywords'),

    html.Br(),
    html.Br(),
    html.Br(), 
    html.Br(),
    html.Br(),
    html.Br(),
    html.Button(id='submit-button', n_clicks=0, children='Submit', style=survey_button_style),
    html.Br(),
    html.Br(),
    html.Br(), 
    html.Br(),
    html.Br(),
    html.Br(),
    dcc.Loading(
        id='loading',
        type='circle',
        children=[html.Div(id="survey-output")]
    ),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),

], style = div_style
)


submit_layout = html.Div([
    html.H1([
    html.Br(),
        html.Br(),
        html.Img(src=app.get_asset_url('thanks.jpg'), style = {'height': '25%', 'width': '25%'})
        ], style = survey_image_style
    ),
    
    html.H1("Cheers! Here's your result.", 
            style=survey_header_font_style),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Iframe(src = "/assets/index.html", height = 900, width = 1200)
])

@app.callback(
    Output("survey-output", "children"),
    [Input('submit-button','n_clicks')],
    [State('keywords', 'value')]
)
def save_user_input(n_clicks, keywords):
    if n_clicks == 1:
        response_json = {'keywords': keywords, 
                            'landed_at': landed_at, 
                            'submitted_at': datetime.utcnow().isoformat()}
        
        collection.insert_one(response_json)
        def pass_user_input(response_json):
            kg.main(response_json['keywords'])
        
        # User has submitted no keywords
        if response_json['keywords'] is None:
            return None
        pass_user_input(response_json)
        
        return submit_layout
if __name__ == '__main__':
    app.run_server(debug=False, host='0.0.0.0')
