# Import Packages

## Standard Packages
from io import StringIO
import math
import getpass
import os
import datetime
import re

## Installed Packages
from rdflib import Graph
from SPARQLWrapper import SPARQLWrapper, JSON, CSV
import networkx as nx
import pandas as pd
import requests
import wikipedia
from neo4j import GraphDatabase, basic_auth
from envparse import env

# Custom Functions
def format_wiki_page(name):
    # Format page name of Wikipedia/DBpedia article for SPARQL
    # From: https://en.wikipedia.org/wiki/Wikipedia:Page_name
    name = name.replace(' ','_')
    name = name.replace('(','\(')
    name = name.replace(')','\)')
    name = name.replace('.', '\.')
    name = name.replace("'", r"\'")
    return name

assert format_wiki_page('Ontology (information science)') == 'Ontology_\(information_science\)'
assert format_wiki_page('Apple Inc.') == 'Apple_Inc\.'
assert format_wiki_page("Grey's Anatomy") == "Grey\\'s_Anatomy"

def format_neo4j_name(name):
    # Format node name for Neo4j
    name = name.replace('(','')
    name = name.replace(')','')
    name = name.replace(' ','_')
    name = name.replace('.','')
    name = name.replace("-", "")
    name = name.replace(" ", "_")
    name = name.replace('"', "'")
    name = re.sub("[^_A-Za-z0-9]", "", name)
    name_match = re.match('[0-9]', name)
    if name_match:
        name = '_'+name
    return name

assert format_neo4j_name('Ontology (information science)') == 'Ontology_information_science'
assert format_neo4j_name('1969 establishments in the United States') == '_1969_establishments_in_the_United_States'

def format_wiki_url(name):
    return name.replace(' ','_')

assert format_wiki_url('Ontology (information science)') == 'Ontology_(information_science)'

def clean_relation(name):
    return name.split('#')[-1].split('/')[-1]

assert clean_relation('http://dbpedia.org/ontology/award') == 'award'
assert clean_relation('http://www.w3.org/2000/01/rdf-schema#seeAlso') == 'seeAlso'

def get_custom_query(keywords):
    # Construct Neo4j query, given keywords
    query = ""
    if len(keywords) == 1:
        keyword = keywords[0].replace('"', "'")
        query += 'MATCH p=(n:Keyword)-->() WHERE n.name = "{0}" RETURN p UNION MATCH p=()-->(n:Keyword) WHERE n.name = "{0}" RETURN p'.format(keyword)
    else:
        for idx, keyword in enumerate(keywords):
            keyword = keyword.replace('"', "'")
            if idx == 0:
                query += 'MATCH p=(n:Keyword)-->() WHERE n.name = "{0}" RETURN p UNION MATCH p=()-->(n:Keyword) WHERE n.name = "{0}" RETURN p '.format(keyword)
            else:
                query += 'UNION MATCH p=(n:Keyword)-->() WHERE n.name = "{0}" RETURN p UNION MATCH p=()-->(n:Keyword) WHERE n.name = "{0}" RETURN p '.format(keyword)
    return query

assert get_custom_query(['Ontology (information science)']) == 'MATCH p=(n:Keyword)-->() WHERE n.name = "Ontology (information science)" RETURN p UNION MATCH p=()-->(n:Keyword) WHERE n.name = "Ontology (information science)" RETURN p'

assert get_custom_query(['Ontology (information science)', 'SQL']) == 'MATCH p=(n:Keyword)-->() WHERE n.name = "Ontology (information science)" RETURN p UNION MATCH p=()-->(n:Keyword) WHERE n.name = "Ontology (information science)" RETURN p UNION MATCH p=(n:Keyword)-->() WHERE n.name = "SQL" RETURN p UNION MATCH p=()-->(n:Keyword) WHERE n.name = "SQL" RETURN p '

def create_index_html(keywords, bolt_websocket_url):
    # Wrapper to generate HTML containing query results
    query = get_custom_query(keywords)
    index_html = """<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
        <title>Neovis.js Simple Example</title>
        <style type="text/css">
            html, body {{
                font: 16pt arial;
            }}
    
            #viz {{
                width: 900px;
                height: 700px;
                border: 1px solid lightgray;
                font: 22pt arial;
            }}
        </style>
        <script src="https://cdn.neo4jlabs.com/neovis.js/v1.5.0/neovis.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script type="text/javascript">
        var viz;
        function draw() {{
            var config = {{
                container_id: "viz",
                server_url: "{1}",
                server_user: "test",
                server_password: "test",
                encrypted: "ENCRYPTION_ON",
                trust: "TRUST_SYSTEM_CA_SIGNED_CERTIFICATES",
                labels: {{
                    "Article": {{
                        "caption": "name"
                    }},
                    "Keyword": {{
                         "caption": "name"
                    }},
                    "Entity": {{
                         "caption": "name"
                    }}
                }},
                relationships: {{
                    "RELATION": {{
                        "caption": false
                    }}
                }},
                initial_cypher: '{0}',
                arrows: true
            }};

            viz = new NeoVis.default(config);
            viz.render();
        }}
    </script>
    </head>
    <body onload="draw()">
        <div id="viz"></div>
    </body>    
</html>""".format(query, bolt_websocket_url)
    with open('src/assets/index.html', 'w') as f:
        f.write(index_html)


# Custom Objects
relation_dict = {'http://dbpedia.org/ontology/wikiPageWikiLink': 'linksTo',
    'http://purl.org/dc/terms/subject': 'hasTopic',
    'http://dbpedia.org/ontology/field': 'ofField',
    'http://dbpedia.org/ontology/knownFor': 'knownFor',
    'http://www.w3.org/2000/01/rdf-schema#seeAlso': 'seeAlso',
    'http://dbpedia.org/ontology/product':'hasProduct'
}


# NYTimes API Key
nyt_api_key = env.str('NYT_API_KEY')
nyt_api_url = 'https://api.nytimes.com/svc/search/v2/articlesearch.json?'

# NewsAPI Key
newsapi_key = env.str('NEWSAPI_KEY')
newsapi_url = 'https://newsapi.org/v2/everything'

# Neo4j Credentials
uri = env.str('NEO4J_URL')
user = env.str('NEO4J_USR')
pwd = env.str('NEO4J_PWD')
bolt_websocket_url = env.str('NEO4J_WEBSOCKET_BOLT_URL')
if bolt_websocket_url.startswith("bolt+s://"):
    bolt_websocket_url = bolt_websocket_url.replace("bolt+s://", "bolt://", 1)


# Disambiguate Keywords
def wikify_keywords(keywords):
    new_keywords = []
    for keyword in keywords:
        new_keyword = wikipedia.search(keyword, results = 1)
        if new_keyword == []:
            continue
        new_keywords.append(new_keyword[0])
    return new_keywords

# Query DBpedia for Keywords
def get_dbpedia_entities(keywords):
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    df = pd.DataFrame()
    for keyword in keywords:
        keyword_wiki = format_wiki_page(keyword)
        keyword_url = format_wiki_url(keyword)
        query = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX dbo: <http://dbpedia.org/ontology/>
    PREFIX dbr: <http://dbpedia.org/resource/>
    PREFIX dct: <http://purl.org/dc/terms/>
    SELECT DISTINCT ?s ?s_label_strip ?p ?o ?o_label_strip
    WHERE 
    {{
        {{
            dbr:{0} ?p  ?o .
            dbr:{0} dct:subject  ?o .
        OPTIONAL {{
            ?o rdfs:label ?o_label .
            BIND(STR(?o_label) AS ?o_label_strip) .
        }}
        }}
    UNION
        {{
            ?s ?p  dbr:{0} .
            ?s dbo:field  dbr:{0} .
        OPTIONAL {{
            ?s rdfs:label ?s_label .
            BIND(STR(?s_label) AS ?s_label_strip) .
            FILTER(LANG(?s_label) = "en") .
        }}
        }}
    UNION
        {{
            dbr:{0} ?p  ?o .
            dbr:{0} rdfs:seeAlso  ?o .
        OPTIONAL {{
            ?o rdfs:label ?o_label .
            BIND(STR(?o_label) AS ?o_label_strip) .
            FILTER(LANG(?o_label) = "en") .
        }}
        }}
    }} 
    """.format(keyword_wiki)
        sparql.setQuery(query)
        sparql.setReturnFormat(CSV)
        results = sparql.query().convert()
        results = results.decode('utf-8')
        df_temp = pd.read_csv(StringIO(results))
        for field in ['s','o']:
            df_temp.loc[df_temp[field].isnull(),field] = 'http://dbpedia.org/resource/'+keyword_url
            df_temp.loc[df_temp['{}_label_strip'.format(field)].isnull(),'{}_label_strip'.format(field)] = keyword
        try:
            df_temp['p_enr'] = df_temp['p'].apply(lambda x: relation_dict[x])
        except Exception as e:
            df_temp['p_enr'] = df_temp['p'].apply(lambda x: clean_relation(x))
        df = df.append(df_temp)
    return df


# Create Graph of Keywords
def create_keyword_graph(G, df, keywords):
    for idx, row in df.iterrows():
        for col in ['s','o']:
            if row['{}_label_strip'.format(col)] not in G.nodes():
                G.add_node(row['{}_label_strip'.format(col)], url=row[col])

        if row['s_label_strip'] != row['o_label_strip']:
            G.add_edge(row['s_label_strip'], row['o_label_strip'], label=row['p_enr'])
    
    # If keyword does not have entry from DBpedia, find corresponding entry in Wikipedia
    for keyword in keywords:
        if keyword not in df['s_label_strip'] and keyword not in df['o_label_strip']:
            wiki_url = wikipedia.page(keyword).url
            G.add_node(keyword, url=wiki_url)
    return G


## Add NYT Articles to Graph
def add_nyt_articles(G, keywords):
    for keyword in keywords:
        print("Keyword:",keyword)
        payload = {'api-key': nyt_api_key, 'q': keyword}
        r = requests.get(nyt_api_url, params=payload)
        article_resp = r.json()['response']['docs']
        for article in article_resp:
            headline = article['headline']['main']
            url = article['web_url']
            G.add_node(headline, url=url, node_type='article')
            G.add_edge(keyword, headline, label='hasArticle')
    return G

## Add News API Articles to Graph
def add_newsapi_articles(G, keywords):
    now = datetime.datetime.now().strftime('%Y-%m-%d')
    thirty_days_ago = (datetime.datetime.now()-datetime.timedelta(days=30)).strftime('%Y-%m-%d')
    headers = {'X-Api-Key': newsapi_key}
    payload = {'from': thirty_days_ago, 'to': now,
                   'sortBy': 'relevancy', 'page': '1',
                   'pageSize': '10'}
    for keyword in keywords:
        print("Keyword:",keyword)
        payload['q'] = keyword
        response = requests.get(newsapi_url, params=payload, headers=headers)
        if response.status_code == 426:
            thirty_days_ago = (datetime.datetime.now()-datetime.timedelta(days=29)).strftime('%Y-%m-%d')
            payload['from'] = thirty_days_ago
            response = requests.get(newsapi_url, params=payload, headers=headers)
        articles = response.json()['articles']
        for article in articles:

            headline = article['title']
            url = article['url']
            source = article['source']['name']
            G.add_node(headline, url=url, source=source, node_type='article')
            G.add_edge(keyword, headline, label='hasArticle')
    return G

# Save to Neo4j
def update_database(G, keywords):
    driver = GraphDatabase.driver(uri, auth=(user, pwd))
    session = driver.session()

    query = ""
    for (name, prop) in G.nodes.data():
        name_attribute = name.replace('"', "'")
        clean_name = format_neo4j_name(name)
        if name in keywords:
            node_type = 'Keyword'
        elif 'node_type' in prop.keys():
            if prop['node_type'] == 'article':
                node_type = 'Article'
        else:
            node_type = 'Entity'
        query += 'CREATE ({2}:{3}  {{name: "{0}", url:"{1}"}})\n'.format(name_attribute,prop['url'],
                                                                         clean_name,node_type)


    for (v1, v2, edgeprop) in G.edges.data():
        v1_clean_name = format_neo4j_name(v1)
        v2_clean_name = format_neo4j_name(v2)
        query += "CREATE ({0})-[:RELATION {{relation:'{2}'}}]->({1})\n".format(v1_clean_name, v2_clean_name, edgeprop['label'])

    session.run(query)
    
    # Merge duplicate records
    labels = ['Article','Entity','Keyword']
    for label in labels:
        query = """MATCH (x:{0}), (y:{0}) 
    WHERE x.name=y.name AND x.url=y.url AND id(x) <> id(y)
    CALL apoc.refactor.mergeNodes([x,y],{{
        properties:"combine",
        mergeRels:true
    }})
    YIELD node 
    RETURN node""".format(label)
        session.run(query)
    
    session.close()


def main(keywords):
    # Disambiguate Keywords
    keywords = wikify_keywords(keywords)
    
    # Query DBpedia for Keywords
    df = get_dbpedia_entities(keywords)
    
    # Create Graph of Keywords
    G = nx.DiGraph()
    G = create_keyword_graph(G, df, keywords)
    
    # Add Articles to Graph
    G = add_newsapi_articles(G, keywords)
    
    # Save to Neo4j
    update_database(G, keywords)
    
    # Create HTML containing graph visualization
    create_index_html(keywords, bolt_websocket_url)

# # TODO:
# 
# - []  Add more relation/predicate types to SPARQL query
# - []  Fix possible HTTP 426 status code error
# - []  Email results when email address is provided

if __name__ == '__main__':
    main(keywords)

